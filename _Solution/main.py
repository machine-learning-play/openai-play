
import openai

from dotenv import load_dotenv
import os

load_dotenv()
OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
FINE_TUNED_MODEL_ID = os.getenv("FINE_TUNED_MODEL_ID")

print( OPENAI_API_KEY )
print( FINE_TUNED_MODEL_ID )

openai.api_key = OPENAI_API_KEY


response = openai.Completion.create(
    engine="text-davinci-003",
    prompt="What is Machine Learning?",
    max_tokens=500,
    model=FINE_TUNED_MODEL_ID
)
